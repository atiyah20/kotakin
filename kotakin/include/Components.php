<?php 
	
	$GLOBALS['ROOT'] = dirname(__DIR__);

	class Components {

		function __construct() {

			session_start();

			$output = "";
			$output .= include_once 'components/header.php';
			$output .= include_once 'components/navigation.php';

			if(empty($_SESSION['email'])) {

				$LOGIN = FALSE;

			} else {

				$LOGIN = TRUE;
				$CUR_EMAIL = $_SESSION['email'];
			}
		}

		function __destruct(){

			$output = "";
			$output .= include_once $GLOBALS['ROOT'].'/components/footer.php';

		}

	}

	/**
	 * 
	 */
	class LandingPage extends Components
	{
		
		function __construct()
		{
			include_once $GLOBALS['ROOT'].'/components/index.php';
		}
	}

	class Marketplace extends Components
	{
		function __construct()
		{
			include_once $GLOBALS['ROOT'].'/components/marketplace.php';
		}

	}

	/**
	 * 
	 */
	class getLoginPage extends components
	{
		
		function __construct()
		{
			include_once $GLOBALS['ROOT'].'/components/masuk.php';
		}
	}

	/**
	 * 
	 */
	class getRegistrationPage extends components
	{
		
		function __construct()
		{
			include_once $GLOBALS['ROOT'].'/components/daftar.php';
		}
	}

	/**
	 * 
	 */
	class getResetPage extends components
	{
		
		function __construct()
		{
			include_once $GLOBALS['ROOT'].'/components/aturulang.php';
		}
	}

	/**
	 * 
	 */
	class getRequestReset extends components
	{
		
		function __construct()
		{
			include_once $GLOBALS['ROOT'].'/components/reset.php';
		}
	}

	/**
	 * 
	 */
	class getCarousel extends components
	{
		
		function __construct()
		{
			include_once $GLOBALS['ROOT'].'/components/carousel.php';
		}
	}

	/**
	 * 
	 */
	class getKedaiInfo extends components
	{
		
		function __construct()
		{
			include_once $GLOBALS['ROOT'].'/components/kedai_info.php';
		}
	}

	function authorizedAccess() {

		header("location:/f/");
	}

	function userRegistered() {
		
		header("location:masuk");
	}

	function redirectToHomepage() {

		header("location:/f/");
	}


 ?>