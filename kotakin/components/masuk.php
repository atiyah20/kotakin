<?php
  
  require_once 'include/db.init.php';

  $alert = "";

  if(isset($_POST['login']))
  {
    $email    = $_POST['email'];
    $password = $_POST['password'];
    
    if ($email == "" || $password == "")
    {
      $alert = '
            <div class="alert alert-danger">
              <p>Kolom harus diisi!</p>
            </div>
      ';
    }
    $login = $koneksi->prepare("SELECT * FROM data_pelanggan WHERE email_pelanggan = ?");
    $login->bindParam(1,$email);
    $login->execute();    
    $fetch_password = $login->fetch();
    $hash_from_database = $fetch_password['password_pelanggan'];
    $logs_email     = $fetch_password['email_pelanggan'];
    
    if (!password_verify($password, $hash_from_database)) {  

      $alert = '

            <div class="alert alert-danger">
              <p>Cek Email atau katasandi!</p>
            </div>
      '; 

    } else { 

      $_SESSION["email"] = $logs_email;

      authorizedAccess();
      
    }   
  }

  if (isset($_SESSION['email'])) {

    authorizedAccess();
  }
?>


  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
          <h2 class="text-center mb-4">Login</h2>
            <div class="auto-form-wrapper">
              <form action="" method="post">
                <div class="form-group">
                  <label class="label">Email</label>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Username" name="email" value="taruna@mail.com">
                  </div>
                </div>
                <div class="form-group">
                  <label class="label">Password</label>
                  <div class="input-group">
                    <input type="password" class="form-control" placeholder="*********" name="password" value="123456">
                  </div>
                </div>
                <div class="form-group">
                  <button name="login" class="btn btn-primary submit-btn btn-block">Login</button>
                </div>
                <div class="form-group d-flex justify-content-between">
                  <div class="form-check form-check-flat mt-0">
            
                    </div>
                    <a href="#" class="text-small forgot-password text-black">Forgot Password</a>
                  </div>
                  <?php echo $alert; ?>
                </form>
                </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
      </div>
      <!-- Footer -->
</div>
