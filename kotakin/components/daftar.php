  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <h2 class="text-center mb-4">Register</h2>
            <div class="auto-form-wrapper">
              <form action="" method="post">
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Nama" name="nama">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Email" name="email">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <input type="password" class="form-control" placeholder="Password" name="password">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <input type="password" class="form-control" placeholder="Confirm Password" name="confirm-password">
                  </div>
                </div>
                <div class="form-group">
                  <button name="daftar" class="btn btn-primary submit-btn btn-block" type="submit">Register</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>

   <?php 

  require_once 'include/db.init.php';
  
  date_default_timezone_set("Asia/Jakarta");

  if(isset($_POST['daftar']))
  {
    $email      = $_POST['email'];
    $nama     = $_POST['nama'];
    $password   = $_POST['password'];
    $password_2   = $_POST['confirm-password'];
    $password_a   = password_hash($password_2, PASSWORD_DEFAULT);
    $id_pelanggan = mt_rand(999999,9999999);
    $tanggal_daftar = date("Y-m-d G:i:s");
    $status_user  = "1";

    if (empty($email || $nama)) {

      echo "Kolom harus diisi semua";
    }

    elseif (empty($password || $password_2)) {

      echo "Buatlah kata sandi untuk akun anda";

    } 

    elseif ($password_2 != $password) {

      echo "Kata sandi harus sama";

    }

    else {
    
      $stmt = $koneksi->prepare("INSERT INTO data_pelanggan (id, nama_pelanggan, email_pelanggan, password_pelanggan, tanggal_daftar, status_pelanggan) VALUES (?,?,?,?,?,?)");
      $stmt->bindParam(1,$id_pelanggan);
      $stmt->bindParam(2,$nama);
      $stmt->bindParam(3,$email);
      $stmt->bindParam(4,$password_a);
      $stmt->bindParam(5,$tanggal_daftar);
      $stmt->bindParam(6,$status_user);
      
      $stmt->execute();
      
      if ($stmt) { 

        userRegistered();

      } else { 

        echo 'Terjadi galat pada server kami';

      }

    } 

  }

  if (isset($_SESSION['email'])) {

    authorizedAccess();
  }

 ?>