  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">BAWAANTAR.COM</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Tentang Kami</a>
            </li>

            <?php 

            if(isset($_SESSION['email'])) {

              echo '
            <li class="nav-item">
              <a class="nav-link" href="keluar.php">Keluar</a>
            </li>

            ';

            } else {

              echo '
            <li class="nav-item">
              <a class="nav-link" href="masuk">Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="daftar">Daftar</a>
            </li>

              ';
            }

             ?>
          </ul>
        </div>
      </div>
    </nav>