<div class="container mb-4">
	<div class="row">
		<div class="col-lg-3">
			<h1>Hari</h1>
			<div class="list-group">
				<a href="marketplace.php?toko=<?php echo htmlspecialchars($_GET['toko']) ?>&hari=1" 
					class="list-group-item">Senin</a>
				<a href="marketplace.php?toko=<?php echo htmlspecialchars($_GET['toko']) ?>&hari=2" 
					class="list-group-item">Selasa</a>
				<a href="marketplace.php?toko=<?php echo htmlspecialchars($_GET['toko']) ?>&hari=3" 
					class="list-group-item">Rabu</a>
				<a href="marketplace.php?toko=<?php echo htmlspecialchars($_GET['toko']) ?>&hari=4" 
					class="list-group-item">Kamis</a>
				<a href="marketplace.php?toko=<?php echo htmlspecialchars($_GET['toko']) ?>&hari=5" 
					class="list-group-item">Jumat</a>
				<a href="marketplace.php?toko=<?php echo htmlspecialchars($_GET['toko']) ?>&hari=6" 
					class="list-group-item">Sabtu</a>
				<a href="marketplace.php?toko=<?php echo htmlspecialchars($_GET['toko']) ?>&hari=7" 
					class="list-group-item">Ahad</a>
			</div>
		</div>
		<div class="col-lg-9">
			<div class="row">
				<?php  
					require_once 'include/db.init.php';
					
					// handler
					$stmt1 = "";
					$success_message = "";
					$error_message = "";
					$false_day = "";
					$dish_selector = "";
					$no_menu_at_the_restaurant = "";

					if(empty($_GET['toko'])) {

						redirectToHomepage();

					} else {

						$restoran = $_GET['toko'];
						$stmt = $koneksi -> prepare("SELECT * FROM data_catering WHERE id_catering = ?");
						$stmt -> bindParam(1, $restoran);
						$stmt -> execute();

						$ctmt = $stmt->rowCount();

						if ($ctmt == 0) {

							$error_message = "We cant find current restaurant";

						} else {

							$success_message = "";
							$data_restoran = $stmt->fetch();
							$nama_restoran = $data_restoran['nama_catering'];

							if (empty($_GET['hari'])) {

								$false_day = "Silahkan pilih hari yang disediakan untuk melihat daftar menu";

							} else {

								$hari = $_GET['hari'];
								$stmt1 = $koneksi -> prepare("SELECT * FROM data_menu WHERE id_catering = ? AND hari = ?");
								$stmt1 -> bindParam(1, $restoran);
								$stmt1 -> bindParam(2, $hari);
								$stmt1 -> execute();

								$ctmt1 = $stmt1->rowCount();

								if($ctmt1 == 0) {

									$no_menu_at_the_restaurant = "Maaf, restoran tersebut sepertinya belum menyediakan menu di hari yang Anda mau.";
									echo  "<h1>Daftar Menu di ".$nama_restoran." </h1>";

								} else {

									require_once 'day_converter.php';
									$hari = "Hari ".$hari;

									echo '<h1>Daftar Menu '.$hari.' '.$nama_restoran.'</h1>';

									while ($menu_restoran = $stmt1->fetch()) {

										//handler
										$nama = $menu_restoran['menu'];
										$id = $menu_restoran['id'];
										$harga = $menu_restoran['harga'];
										$deskripsi = $menu_restoran['deskripsi_menu'];


										echo '
										<div class="col-lg-4 col-md-6 mb-4">
										<div class="card h-100">
											<a href="">
												<img src="http://placehold.it/700x400" 
												class="card-img-top">
											</a>
											<div class="card-body">
												<h4 class="card-title">
													<a href="" class="card-text">'.$nama.'</a>
												</h4>
												<h5>'.$harga.'</h5>
												<p class="card-text">Menu content</p>
											</div>
											<div class="card-footer">
												<a href="?toko='.$_GET['toko'].'&hari='.$_GET['hari'].'&action=add&id='.$id.'">Pesan</a>
											</div>
										</div>
										</div>
										';

									}
								}

							}

						}
					}
				?>
				
				<div class="container">
					<h3 class="text-danger"><?php echo $error_message ?></h3>
					<h3 class="text-success"><?php echo $success_message ?></h3>
					<h3 class="text-success"><?php echo $false_day ?></h3>
					<h3 class="text-danger"><?php echo $no_menu_at_the_restaurant ?></h3>
				</div>
				<hr>
				<?php require_once 'cart.php'; ?>
			</div>
		</div>
	</div>
</div>