<?php 
	
	require_once 'database.php';

	$query = $koneksi->prepare("SELECT * FROM catering");
	$query->execute();

	while($catering = $query->fetch()){
		$item[] = array(
			"makanan"=>$catering['makanan_menu'],
			"harga"=>$catering['harga_menu'],
			"deskripsi"=>$catering['deskripsi']
		);
	}

	$json = array(
		'result' => 'Success',
		'item' => $item
	);

	echo json_encode($json);
 ?>